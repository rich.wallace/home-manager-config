{
  inputs,
  pkgs,
  ...
}:
with inputs; let
  neovimModule = {
    programs.neovim.enable = pkgs.lib.mkForce false;
    programs.bash.shellAliases.vimdiff = "nvim -d";

    home = {
      packages = [
        (inputs.neovim-flake.lib.neovimConfiguration {
          # TODO: possible other plugins to add
          # * nvim-ts-rainbow2 (not easy, treesitter addon)
          # * toggleterm
          # * neotest (neotest-haskell)
          #
          # * schemastore
          # * luasnip
          # * nvim-dap (nvim-dap-ui, telescope-dap, nvim-dap-virtual-text)

          inherit pkgs;

          modules = [
            # haskell-tools module, similar to rust-tools
            {
              config.build.rawPlugins.haskell-tools.src = inputs.haskell-tools-nvim;
            }
            (
              {
                pkgs,
                config,
                lib,
                ...
              }:
                with lib;
                with builtins; let
                  cfg = config.vim.languages.haskell;
                in {
                  options.vim.languages.haskell = {
                    enable = mkEnableOption "Haskell language support";

                    lsp = {
                      enable = mkOption {
                        description = "Haskell LSP support (haskell-language-server)";
                        type = types.bool;
                        default = config.vim.languages.enableLSP;
                      };
                      # package = mkOption {
                      #   description = "haskell-language-server package";
                      #   type = types.package;
                      #   default = pkgs.haskell-language-server;
                      # };
                      # opts = mkOption {
                      #   description = "Options to pass to rust analyzer";
                      #   type = types.str;
                      #   default = "";
                      # };
                    };

                    treesitter = {
                      enable = mkOption {
                        description = "Enable Haskell treesitter";
                        type = types.bool;
                        default = config.vim.languages.enableTreesitter;
                      };
                      package = nvim.types.mkGrammarOption pkgs "haskell";
                    };
                  };

                  config = mkIf cfg.enable (mkMerge [
                    (mkIf cfg.treesitter.enable {
                      vim.treesitter.enable = true;
                      vim.treesitter.grammars = [cfg.treesitter.package];
                    })

                    (mkIf cfg.lsp.enable {
                      vim = {
                        startPlugins = ["haskell-tools"];
                        ftplugins.haskell = ''
                          local ht = require('haskell-tools')
                          local def_opts = { noremap = true, silent = true, }
                          ht.start_or_attach {
                            hls = {
                              on_attach = function(client, bufnr)
                                require('flake/lsp').default_on_attach(client, bufnr)
                                local opts = vim.tbl_extend('keep', def_opts, { buffer = bufnr, })
                                -- haskell-language-server relies heavily on codeLenses,
                                -- so auto-refresh (see advanced configuration) is enabled by default
                                vim.keymap.set('n', '<space>ca', vim.lsp.codelens.run, opts)
                                vim.keymap.set('n', '<space>hs', ht.hoogle.hoogle_signature, opts)
                                vim.keymap.set('n', '<space>ea', ht.lsp.buf_eval_all, opts)
                              end,
                            },
                          }

                          -- Suggested keymaps that do not depend on haskell-language-server:
                          local bufnr = vim.api.nvim_get_current_buf()
                          -- set buffer = bufnr in ftplugin/haskell.lua
                          if vim.bo[bufnr].filetype == "haskell" then
                            local opts = { noremap = true, silent = true, buffer = bufnr }

                            -- Toggle a GHCi repl for the current package
                            vim.keymap.set('n', '<leader>rr', ht.repl.toggle, opts)
                            -- Toggle a GHCi repl for the current buffer
                            vim.keymap.set('n', '<leader>rf', function()
                              ht.repl.toggle(vim.api.nvim_buf_get_name(0))
                            end, def_opts)
                            vim.keymap.set('n', '<leader>rq', ht.repl.quit, opts)

                            -- Detect nvim-dap launch configurations
                            -- (requires nvim-dap and haskell-debug-adapter)
                            -- ht.dap.discover_configurations(bufnr)
                          end
                        '';
                      };
                    })
                  ]);
                }
            )

            # purescript module
            {
              config.build.rawPlugins.purescript-vim.src = inputs.purescript-vim;
            }
            (
              # TODO: add config settings as options
              # TODO: treesitter?
              {
                pkgs,
                config,
                lib,
                ...
              }:
                with lib;
                with builtins; let
                  cfg = config.vim.languages.purescript;
                  defaultServer = "purescriptls";
                  servers = {
                    purescriptls = {
                      lspConfig = ''
                        require('lspconfig').purescriptls.setup {
                          on_attach = require('flake/lsp').default_on_attach,
                          settings = {
                            purescript = {
                              addSpagoSources = true,
                              addNpmPath = true,
                            }
                          },
                          flags = {
                            debounce_text_changes = 150,
                          },
                        }
                      '';
                    };
                  };
                in {
                  options.vim.languages.purescript = {
                    enable = mkEnableOption "PureScript language support";

                    lsp = {
                      enable = mkOption {
                        description = "PureScript LSP support (purescript-language-server)";
                        type = types.bool;
                        default = config.vim.languages.enableLSP;
                      };
                      server = mkOption {
                        description = "PureScript LSP server to use";
                        type = with types; enum (attrNames servers);
                        default = defaultServer;
                      };
                      # package = mkOption {
                      #   description = "haskell-language-server package";
                      #   type = types.package;
                      #   default = pkgs.haskell-language-server;
                      # };
                    };

                    # treesitter = {
                    #   enable = mkOption {
                    #     description = "Enable PureScript treesitter";
                    #     type = types.bool;
                    #     default = config.vim.languages.enableTreesitter;
                    #   };
                    #   package = nvim.types.mkGrammarOption pkgs "purescript";
                    # };
                  };

                  config = mkIf cfg.enable (mkMerge [
                    {
                      vim.startPlugins = ["purescript-vim"];
                    }
                    # (mkIf cfg.treesitter.enable {
                    #   vim.treesitter.enable = true;
                    #   vim.treesitter.grammars = [cfg.treesitter.package];
                    # })

                    (mkIf cfg.lsp.enable {
                      # TODO: for some reason putting this in a ftplugin/purescript.lua doesn't cause the
                      # buffer to attach correctly. For now we'll keep in the init.vm but it would
                      # be nice to figure out the problem
                      vim.lsp.lspconfig.sources.purescriptls = servers.${cfg.lsp.server}.lspConfig;
                    })
                  ]);
                }
            )

            # Comment.nvim module
            {
              config.build.rawPlugins.comment-nvim.src = inputs.comment-nvim;
            }
            (
              # TODO: add settings as options
              {
                pkgs,
                config,
                lib,
                ...
              }:
                with lib;
                with builtins; let
                  cfg = config.vim.comment;
                in {
                  options.vim.comment = {
                    enable = mkEnableOption "Enable Comment.nvim";
                  };

                  config = mkIf cfg.enable {
                    vim.startPlugins = ["comment-nvim"];
                    vim.luaConfigRC.nvimBufferline = nvim.dag.entryAnywhere ''
                      require("Comment").setup {}
                    '';
                  };
                }
            )

            # persisted.nvim module
            {
              config.build.rawPlugins.persisted-nvim.src = inputs.persisted-nvim;
            }
            (
              # TODO: add settings as options
              {
                pkgs,
                config,
                lib,
                ...
              }:
                with lib;
                with builtins; let
                  cfg = config.vim.persisted;
                in {
                  options.vim.persisted= {
                    enable = mkEnableOption "Enable persisted.nvim";
                  };

                  config = mkIf cfg.enable {
                    vim.startPlugins = ["persisted-nvim"];
                    vim.luaConfigRC.nvimBufferline = nvim.dag.entryAnywhere ''
                      require("persisted").setup {}
                    '';
                  };
                }
            )

            # todo-comments.nvim module
            {
              config.build.rawPlugins.todo-comments-nvim.src = inputs.todo-comments-nvim;
            }
            (
              # TODO: add settings as options
              {
                pkgs,
                config,
                lib,
                ...
              }:
                with lib;
                with builtins; let
                  cfg = config.vim.todo-comments;
                in {
                  options.vim.todo-comments = {
                    enable = mkEnableOption "Enable todo-comments.nvim";
                  };

                  config = mkIf cfg.enable {
                    vim.startPlugins = ["todo-comments-nvim"];
                    vim.luaConfigRC.nvimBufferline = nvim.dag.entryAnywhere ''
                      require("todo-comments").setup {}
                    '';
                  };
                }
            )

            # trim.nvim module
            {
              config.build.rawPlugins.trim-nvim.src = inputs.trim-nvim;
            }
            (
              # TODO: add settings as options
              {
                pkgs,
                config,
                lib,
                ...
              }:
                with lib;
                with builtins; let
                  cfg = config.vim.trim;
                in {
                  options.vim.trim = {
                    enable = mkEnableOption "Enable trim.nvim";
                  };

                  config = mkIf cfg.enable {
                    vim.startPlugins = ["trim-nvim"];
                    vim.luaConfigRC.nvimBufferline = nvim.dag.entryAnywhere ''
                      require("trim").setup {}
                    '';
                  };
                }
            )

            {
              vim = {
                tabWidth = 2;
              };
            }

            {
              # basic config, based on a fairly minimal nix,html,sql,markdown config
              build = {
                vimAlias = true;
                viAlias = true;
              };

              vim = {
                autopairs.enable = true;
                autocomplete = {
                  enable = true;
                  type = "nvim-cmp";
                };

                comment.enable = true;
                filetree.nvimTreeLua.enable = true;

                git = {
                  enable = true;
                  gitsigns.enable = true;
                };

                keys = {
                  enable = true;
                  whichKey.enable = true;
                };

                languages = {
                  enableLSP = true;
                  enableFormat = true;
                  enableTreesitter = true;
                  enableExtraDiagnostics = true;

                  haskell.enable = true;
                  html.enable = true;
                  markdown.enable = true;
                  nix.enable = true;
                  purescript.enable = true;
                  # sql.enable = true;
                };

                lsp = {
                  fidget.enable = true;
                  # formatOnSave = true;
                  lspkind.enable = true;
                  lspSignature.enable = true;
                  lightbulb.enable = false;
                  # TODO figure out why lspsaga is broken
                  # lspsaga.enable = true;
                  nvimCodeActionMenu.enable = true;
                  trouble.enable = true;
                };

                persisted.enable = true;

                statusline.lualine.enable = true;
                tabline.nvimBufferline.enable = true;
                telescope.enable = true;
                theme.enable = true;

                todo-comments.enable = true;
                treesitter.context.enable = true;
                trim.enable = true;

                visuals = {
                  enable = true;

                  cursorWordline = {
                    enable = true;
                    lineTimeout = 0;
                  };
                  indentBlankline = {
                    enable = true;
                    fillChar = null;
                    eolChar = null;
                    showCurrContext = true;
                  };
                  nvimWebDevicons.enable = true;
                };
              };
            }
          ];
        })
      ];
      sessionVariables.EDITOR = "nvim";
    };
  };
in {
  imports = [
    ./modules/services/skhd.nix
    ./modules/services/yabai.nix
    "${dot-files}/modules/home-manager/machine-profiles/darwin.nix"
    ./well.nix
    # ./neovim.nix
    neovimModule
  ];

  home = {
    username = "rich.wallace";
    homeDirectory = "/Users/rich.wallace";
    sessionVariables = {
      AWS_PROFILE = "development-platform-services";
      AWS_DEFAULT_REGION = "us-east-2";
      NIX_PATH = "nixpkgs=${nixpkgs.outPath}:home-manager=${home-manager.outPath}";
    };
    stateVersion = "22.05";

    packages = with pkgs; [
      watch
      watchexec
    ];
  };

  nix = {
    registry.nixpkgs.flake = inputs.nixpkgs;
  };

  programs = {
    git = {
      userEmail = nixpkgs.lib.mkForce "rich.wallace@well.co";
    };
  };

  services = {
    skhd = {
      enable = true;

      keybindings = ''
        cmd - return : open -na kitty
        shift + cmd - s : screencapture -c -d -i
        ctrl + cmd - p : rbw get login.microsoftonline.com rich.wallace@well.co | pbcopy

        cmd - f4 : yabai -m window --close

        # move managed window
        cmd - left  : yabai -m window --focus west
        cmd - down  : yabai -m window --focus south
        cmd - up    : yabai -m window --focus north
        cmd - right : yabai -m window --focus east

        shift + cmd - left  : yabai -m window --warp west
        shift + cmd - down  : yabai -m window --warp south
        shift + cmd - up    : yabai -m window --warp north
        shift + cmd - right : yabai -m window --warp east

        alt - v : yabai -m window --insert south
        alt - h : yabai -m window --insert east

        # balance size of windows
        shift + alt - 0 : yabai -m space --balance

        # fast focus desktop
        # don't work without scripting extension
        # cmd + alt - x : yabai -m space --focus recent
        # cmd - 1 : yabai -m space --focus 1
        # cmd - 2 : yabai -m space --focus 2
        # cmd - 3 : yabai -m space --focus 3
        # cmd - 4 : yabai -m space --focus 4
        # cmd - 5 : yabai -m space --focus 5
        # cmd - 6 : yabai -m space --focus 6
        # cmd - 7 : yabai -m space --focus 7
        # cmd - 8 : yabai -m space --focus 8
        # cmd - 9 : yabai -m space --focus 9
        # cmd - 0 : yabai -m space --focus 10

        shift + cmd - 1 : yabai -m window --space 1
        shift + cmd - 2 : yabai -m window --space 2
        shift + cmd - 3 : yabai -m window --space 3
        shift + cmd - 4 : yabai -m window --space 4
        shift + cmd - 5 : yabai -m window --space 5
        shift + cmd - 6 : yabai -m window --space 6
        shift + cmd - 7 : yabai -m window --space 7
        shift + cmd - 8 : yabai -m window --space 8
        shift + cmd - 9 : yabai -m window --space 9
        shift + cmd - 0 : yabai -m window --space 10

        # toggle window zoom
        # alt - d : yabai -m window --toggle zoom-parent
        alt - f : yabai -m window --toggle zoom-fullscreen
        shift + alt - f : yabai -m window --toggle native-fullscreen

        # toggle window split type
        # alt - e : yabai -m window --toggle split

        # float / unfloat window and center on screen
        shift + cmd - t : yabai -m window --toggle float;\
                  yabai -m window --grid 4:4:1:1:2:2
      '';
    };

    yabai = {
      enable = true;
      config = {
        active_window_border_color = "0xffb2b2b2";
        focus_follows_mouse = "autoraise";
        layout = "bsp";
        mouse_modifier = "fn";
        mouse_action1 = "move";
        mouse_action2 = "resize";
        window_placement = "second_child";
        window_border = "off";
        window_shadow = "off";
      };
      extraConfig = ''
        yabai -m rule --add app="^System Preferences$" manage=off
      '';
    };
  };
}
