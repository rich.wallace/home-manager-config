{ pkgs, ... }:

{
  fonts = {
    fontDir.enable = true;
    fonts = [
      (pkgs.nerdfonts.override { fonts = [ "FiraCode" "DejaVuSansMono" ]; })
    ];
  };

  nix = {
    settings = {
      build-users-group = "nixbld";
      trusted-users = ["rich.wallace"];
      sandbox = false;
    };
    extraOptions = ''
      # experimental-features = nix-command flakes ca-derivations
      experimental-features = nix-command flakes
    '';
  };

  services.nix-daemon.enable = true;
}
