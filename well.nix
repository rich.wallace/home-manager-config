{ inputs, pkgs, ... }:

let
  inherit (import inputs.health-engine-development-tools-src { inherit (pkgs) system; }) health-engine-development-tools;
in
{
  home = {
    packages = [ pkgs.nix-output-monitor health-engine-development-tools ];
  };

  nix = {
    settings = {
      trusted-public-keys = [
        "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
        "well.co-1:IZB8nni3k2tpJQGL/keiGA8kkWV8niJd+wvwoKYdYzA="
        "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ="
      ];
      trusted-substituters = [
        "https://cache.nixos.org"
        "s3://well-nix-cache?region=us-east-2&profile=development-platform-services"
        "https://cache.iog.io"
      ];
      substituters = [
        "https://cache.nixos.org"
        "s3://well-nix-cache?region=us-east-2&profile=development-platform-services"
        "https://cache.iog.io"
      ];
    };
  };

  nixpkgs = {
    overlays = [
      (self: super: {
        haskellPackages = super.haskellPackages.override {
          overrides = hself: hsuper:
            let
              # Workaround for https://github.com/NixOS/nixpkgs/issues/140774
              fixCyclicReference = drv:
                super.haskell.lib.overrideCabal drv (_: {
                  enableSeparateBinOutput = false;
                });
            in {
              ghcid = fixCyclicReference hsuper.ghcid;
              haskell-language-server = hsuper.haskell-language-server.overrideScope (lself: lsuper: {
                ormolu = fixCyclicReference hsuper.ormolu;
              });
              niv = fixCyclicReference hsuper.niv;
            };
        };
      })
    ];
  };

  programs = {
    bash = {
      profileExtra = ''
        source ${health-engine-development-tools}/lib/ah_aws.sh
      '';
    };
  };
}
