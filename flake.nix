{
  inputs = {
    darwin = {
      url = "github:LnL7/nix-darwin";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    dot-files = {
      url = "github:purefn/dot-files";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.home-manager.follows = "home-manager";
    };

    health-engine-development-tools-src = {
      url = "git+ssh://git@gitlab.com/welllabs/health-engine/development-tools.git?rev=fdf8c74ef77bf469b814a080ce5104162527f8d6";
      flake = false;
    };

    home-manager = {
      url = "github:nix-community/home-manager";
    };

    nixpkgs.follows = "home-manager/nixpkgs";

    neovim-flake = {
      # url = "github:jordanisaacs/neovim-flake";
      url = "path:/Users/rich.wallace/src/purefn/neovim-flake";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    haskell-tools-nvim = {
      url = "github:mrcjkb/haskell-tools.nvim/1.x.x";
      flake = false;
    };

    purescript-vim = {
      url = "github:purescript-contrib/purescript-vim";
      flake = false;
    };

    comment-nvim = {
      url = "github:numToStr/Comment.nvim";
      flake = false;
    };

    persisted-nvim = {
      url = "github:olimorris/persisted.nvim";
      flake = false;
    };

    todo-comments-nvim = {
      url = "github:folke/todo-comments.nvim";
      flake = false;
    };

    trim-nvim = {
      url = "github:cappyzawa/trim.nvim";
      flake = false;
    };

    # nvim-ts-rainbow2 = {
    #   url = "HiPhish/nvim-ts-rainbow2";
    #   flake = false;
    # };
  };

  outputs = inputs @ {
    self,
    darwin,
    home-manager,
    nixpkgs,
    neovim-flake,
    ...
  }: let
    system = "aarch64-darwin";
    pkgs = nixpkgs.legacyPackages.${system};
    # switch = pkgs.writeShellScript "switch" ''
    #   ${darwin.packages.${system}.darwin-rebuilt}/bin/darwin-rebuild \
    #     switch --flake .#mbp
    # '';
    switch = pkgs.writeShellScript "switch" ''
      ${home-manager.packages.${system}.home-manager}/bin/home-manager \
        switch --flake .#rwallace
    '';
    update = pkgs.writeShellScript "update" ''
      ${pkgs.nix}/bin/nix flake update
      ${switch}
    '';
  in {
    apps.${system} = {
      default = {
        type = "app";
        program = "${switch}";
      };
      switch = {
        type = "app";
        program = "${switch}";
      };
      update = {
        type = "app";
        program = "${update}";
      };
    };
    darwinConfigurations =
      let cfg = darwin.lib.darwinSystem {
        inherit system;
	modules = [./darwin.nix];
      };
      in {
        WDKGC0XH1H = cfg;
        Q2RWCFYQG3 = cfg;
      };
    homeConfigurations = {
      rwallace = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;

        # makes this get passed to modules, like well.nix
        extraSpecialArgs = {inherit inputs;};

        modules = [./home.nix];
      };
    };
  };
}
